/*
    Based on an Example sketch By Ivan Grokhotov
    Adapted to work with setInsecure() rather than a fingerprint
    supply your own API, phonenumber, and login credentials
    Project description: https://arduinodiy.wordpress.com/2021/07/14/sending-messages-through-signal/
*/

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <credentials.h>

const char*  ssid = "yourSSID";
const char* password = "yourPW";

const char* host = "api.callmebot.com";
const int httpsPort = 443;

// Use web browser to view and copy
// SHA1 fingerprint of the certificate
//const char* fingerprint = "35 85 74 EF 67 35 A7 CE 40 69 50 F3 C0 F6 80 CF 80 3B 2E 19";
//const char* fingerprint = "5f b7 ee 06 33 e2 59 db ad 0c 4c 9a e6 d3 8f 1a 61 c7 dc 25";
const char* fingerprint = "5f f1 60 31 09 04 3e f2 90 d2 b0 8a 50 38 04 e8 37 9f bc 76"; //irrelevant

void setup() {
  Serial.begin(115200);
  Serial.println();
  Serial.print("connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // Use WiFiClientSecure class to create TLS connection
  WiFiClientSecure client;
  client.setInsecure(); //
  Serial.print("connecting to ");
  Serial.println(host);
  if (!client.connect(host, httpsPort)) {
    Serial.println("connection failed");
    return;
  }

  if (client.verify(fingerprint, host)) {
    Serial.println("certificate matches");
  } else {
    Serial.println("certificate doesn't match");
  }

  // String url = "/signal/send.php?phone=+31612345678&apikey=498765&image=https%3A%2F%2Fviraaltjesnl.bladecdn.net%2Fwp-content%2Fuploads%2F2019%2F08%2Fjutteke2-1.png&f=1&nofb=1";
  String url = "/signal/send.php?phone=+31612345678&apikey=498765&text=succes"; //-> For Signal app
  //Use the belowString for whatsapp messages
  //String url="/whatsapp.php?phone=+3161234567&text=Hi+there&apikey=876543"; // -> For WhatsApp app
  //use the belowstring for telegram
  //String url="/text.php?user=@JohDoe&text=This+is+a+test+from+CallMeBot"; // -> For Telegram app
  Serial.print("requesting URL: ");
  Serial.println(url);

  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "User-Agent: BuildFailureDetectorESP8266\r\n" +
               "Connection: close\r\n\r\n");

  Serial.println("request sent");
  while (client.connected()) {
    String line = client.readStringUntil('y'); //checking for a not relevant character, apparently some timing issue
    if (line == "\r") {
      Serial.println("headers received");
      break;
    }
  }
  String line = client.readStringUntil('\n');
  if (line.startsWith("Apikey")) {
   // Serial.println("esp8266/Arduino CI successfull!");//irrelevant
  } else {
    //Serial.println("esp8266/Arduino CI has failed");//irrelevant
  }
  Serial.println("reply was:");
  Serial.println("==========");
  Serial.println(line);
  Serial.println("==========");
  Serial.println("closing connection");
}

void loop() {
}
